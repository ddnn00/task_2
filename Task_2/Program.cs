﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputHooks = Console.ReadLine();

            Stack<char> hooks = new Stack<char>();

            var dict = new Dictionary<char, char>()
            {
                { ')', '(' },
                { ']', '[' },
                { '}', '{' },
            };

            if (!Regex.IsMatch(inputHooks, @"^([\[\]\{\}\(\)]+)$"))
            {
                Console.WriteLine("Вы ввели не скобки!");
                return;
            }
            foreach (var ch in inputHooks)
            {
                
                if (ch == '(' || ch == '[' || ch == '{')
                {
                    hooks.Push(ch);
                }
                else
                {
                    if(hooks.Count == 0)
                    {
                        Console.WriteLine("false");
                        return;
                    }
                    if (dict[ch] == hooks.Peek())
                    {
                        hooks.Pop();
                    }
                }
            }
            Console.WriteLine(hooks.Count > 0 ? "false" : "true");
        }
    }
}
